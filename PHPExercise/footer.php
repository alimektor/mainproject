<?php
/**
 * Created by PhpStorm.
 * User: Alime
 * Date: 18.05.2017
 * Time: 12:34
 */
?>
<!-- FOOTER START -->
<footer>
    <div class="row">
        <div class="col-md-4">
            <h3>Sitemap</h3>
            <?php
            OutputPages::outputMenuFooter($page);
            ?>
        </div>
        <div class="col-md-4">
            <h3>Sitemap</h3>
            <a href="#">Home</a>
            <a href="#">About</a>
            <a href="#">Services</a>
        </div>
        <div class="col-md-4">
            <h3>Copyright</h3>
            <p>
                © 2017
            </p>
        </div>
    </div>
</footer>
<!-- FOOTER END -->
</div>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.js"></script>
</body>
</html>
