<?php

/**
 * Created by PhpStorm.
 * User: Alime
 * Date: 18.05.2017
 * Time: 12:42
 */
class OutputPages
{
    /**
     * @param array $page - Массив основных данных сайта
     */
    static function outputMenu(array $page)
    {
        foreach ($page as $key => $item) {

            echo "<li><a href=\"$key\">$item[Title]</a></li>";
        }
    }

    static function outputMenuFooter(array $page)
    {
        foreach ($page as $key => $item) {

            echo "<a href=\"$key\"> $item[Title] </a>";
        }
    }

    /**
     * @param $page - Получает методом GET значение страницы.
     */
    static function outputContent($get_GET, $page)
    {
        foreach ($page as $item) {
            if ($item['GET'] == $get_GET) {
                readfile("files/$item[Content]");
            }
        }
    }
    /**
     * @param $get_GET - Получение метода GET
     * @param array $page - Массив основных данных сайта
     */
    static function outputTitle($get_GET, array $page)
    {
        foreach ($page as $item) {
            if ($item['GET'] == $get_GET) {
                echo $item['Title'];
            }
        }
    }
}

?>

