<?php
/**
 * Created by PhpStorm.
 * User: Alime
 * Date: 18.05.2017
 * Time: 12:24
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>
        <?php
        OutputPages::outputTitle(@$_GET['page'], $page);
        ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
<!-- NAVIGATION START -->
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button> <a class="navbar-brand" href="#">Типа лого</a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <?php
                        OutputPages::outputMenu($page);
                        ?>
                    </ul>
                    <form class="navbar-form navbar-right form-search" role="search">
                        <div class="input-group">
                            <div class="form-group">
                                <input type="text" class="form-control input-medium search-query" placeholder="Поиск по сайту">
                            </div>
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">
                                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            </button>
                            </span>
                        </div>

                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a data-toggle="modal" data-target="#login_modal">Авторизация</a>
                        </li>
                        <li>
                            <a data-toggle="modal" data-target="#registration_modal">Регистрация</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

<!-- NAVIGATION END -->
