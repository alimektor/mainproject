<?php
/**
 * Created by PhpStorm.
 * User: Alime
 * Date: 18.05.2017
 * Time: 12:28
 */
?>
<!-- MODAL FORMS START -->
<!-- LOGIN -->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="LoginLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="LoginLabel">Авторизация</h4>
            </div>
            <div class="modal-body">
                <form class="form" role="form" method="post" action="#" accept-charset="UTF-8" id="login-nav">
                    <div class="form-group">
                        <input type="email" class="form-control" id="InputEmail" placeholder="Адрес электронной почты" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="InputPassword" placeholder="Пароль" required>
                        <div class="help-block"><a href="">Забыли пароль?</a></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Войти</button>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox">
                            Запомнить меня
                        </label>
                    </div>
                </form>
                <div class="bottom text-right">
                    Впервые на сайте?
                    <a href="#"><b>Регистрация</b></a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- LOGIN -->
<!-- REGISTRATION -->
<div class="modal fade" id="registration_modal" tabindex="-1" role="dialog" aria-labelledby="RegistrationLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="RegistrationLabel">Регистрация</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="LastName">Фамилия:</label>
                        <div class="col-xs-9">
                            <input type="text" name="last_name" class="form-control" id="LastName" placeholder="Введите фамилию" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="FirstName">Имя:</label>
                        <div class="col-xs-9">
                            <input type="text" name="first_name" class="form-control" id="FirstName" placeholder="Введите имя" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="Patronymic">Отчество:</label>
                        <div class="col-xs-9">
                            <input type="text" name="patronymic" class="form-control" id="Patronymic" placeholder="Введите отчество">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Дата рождения:</label>
                        <div class="col-xs-3">
                            <select name="day_birth" class="form-control">
                                <option>День</option>
                            </select>
                        </div>
                        <div class="col-xs-3">
                            <select name="month_birth" class="form-control">
                                <option>Месяц</option>
                            </select>
                        </div>
                        <div class="col-xs-3">
                            <select name="year_birth" class="form-control">
                                <option>Год</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="InputEmailReg">E-mail:</label>
                        <div class="col-xs-9">
                            <input type="email" name="email" class="form-control" id="InputEmailReg" placeholder="Введите Ваш адрес электронной почты" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="InputPasswordReg">Пароль:</label>
                        <div class="col-xs-9">
                            <input type="password" name="password" class="form-control" id="InputPasswordReg" placeholder="Введите пароль" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="ConfirmPassword">Подтвердите пароль:</label>
                        <div class="col-xs-9">
                            <input type="password" name="confrirm_password" class="form-control" id="ConfirmPassword" placeholder="Введите пароль ещё раз" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="PostalAddress">Адрес:</label>
                        <div class="col-xs-9">
                            <textarea name="postal_address" rows="4" class="form-control" id="PostalAddress" placeholder="Введите адрес"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Пол:</label>
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="GenderRadio" value="male"> Мужской
                            </label>
                        </div>
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="GenderRadio" value="female"> Женский
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-9">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="agree" required>  Я согласен с <a href="#">условиями</a>.
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-9">
                            <input type="submit" class="btn btn-primary" value="Регистрация">
                            <input type="reset" class="btn btn-default" value="Очистить форму">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- REGISTRATION -->
<!-- MODAL FORMS END -->