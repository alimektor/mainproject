<?php
/**
 * Created by PhpStorm.
 * User: Alime
 * Date: 19.05.2017
 * Time: 10:45
 */
$page = array(
    'index.php' => array(
        'Title' => 'Главная',
        'Content' => "main.html",
        'GET' => NULL
    ),
    'index.php?page=about_us' => array(
        'Title' => 'О нас',
        'Content' => "about_us.html",
        'GET' => 'about_us'
    ),
    'index.php?page=help' => array(
        'Title' => 'Помощь',
        'Content' => "help.html",
        'GET' => 'help'
    )
);
?>